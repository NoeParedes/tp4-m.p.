import os

def menu():
    print('++++MENU++++')
    print('a) Registrar productos: código, descripción, precio y stock.')
    print('b) Mostrar el listado de productos.')
    print('c) Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]')
    print('d) Sumar X al stock de todos los productos cuyo valor actual de stock sea menor al valor Y.')
    print('e) Eliminar todos los productos cuyo stock sea igual a cero. ')
    print('f) Salir')
    eleccion = input('Elija una opción: ')
    return eleccion

def leerPrecio1():
    while True :
        precio = input('Precio: ')
        try:
            precio = int(precio)
            return precio   
        except ValueError :
            print('La entrada es incorrecta: escribe un precio')

def leerPrecio():
    precio = leerPrecio1()
    while not((precio >= 0) ):
        precio = leerPrecio1()
    return precio    

def leerStock1():
    while True :
        stock = input('Stock: ')
        try:
            stock = int(stock)
            return stock   
        except ValueError :
            print('La entrada es incorrecta: escribe stock')

def leerStock():
    stock = leerStock1()
    while not((stock >= 0) ):
        stock = leerStock1()
    return stock

def leerCodigo1():
    while True :
        codigo = input('Codigo: ')
        try:
            codigo = int(codigo)
            return codigo   
        except ValueError :
            print('La entrada es incorrecta: escribe un codigo')

def leerCodigo():
    codigo = leerCodigo1()
    while not((codigo >= 0) ):
        codigo = leerCodigo1()
    return codigo

def leerHasta(desde):
    hasta = int(input('Ingrese Stock hasta: '))
    while not((hasta >= desde) ):
        hasta= int(input('Ingrese Stock nuevamente hasta: '))
    return hasta

def leerProductos():
    productos = {}
    c='s'
    while (c=='s' or c=='S'):
        codigo = leerCodigo()
        if codigo not in productos:    
                descripcion = input('Descripcion: ')
                precio = leerPrecio()
                stock = leerStock()
                productos[codigo] = [descripcion,precio,stock]
                print('Agregado correctamente') 
        else:
            print('El producto ya existe')
        c=input('¿Desea seguir ingresando productos? s/n:  ')              
    return productos

def mostrar(productos):
    for clave, valor in productos.items():
        print(clave,valor)

def mostrarStock(productos):
    desde=int(input('Ingrese Stock desde: '))
    hasta=leerHasta(desde)
    for clave, valor in productos.items():
        if(productos[clave][2]>=desde) and (productos[clave][2]<=hasta):
            print(clave,valor)

def eliminar(productos):
    i=1
    while i<= len(productos):
        for codigo, valor in productos.items():
            if(valor[2]==0):
                print('Se elimina el producto: ',productos[codigo])
                del productos[codigo]
                break
        i+=1
    return productos        

def modificar(productos):
    x=int(input('Ingrese x: '))   
    y=int(input('Ingrese y: '))   
    for clave, valor in productos.items():  
        if(valor[2] < y):
            valor[2]=valor[2]+x
            productos[clave] = [valor]
    print('Modificado correctamente')
    return productos

#principal
opcion = ''
while (opcion != 'f'):
    os.system("cls")
    opcion = menu()
    if opcion == 'a':
        print("+++REGISTRO PRODUCTOS+++")
        productos = leerProductos()
    elif opcion == 'b':
        print("+++LISTA DE PRODUCTOS+++")
        mostrar(productos)
    elif opcion == 'c':
        print("+++LISTA DE PRODUCTOS CON STOCK+++")
        mostrarStock(productos)
    elif opcion == 'd':
        print("++MODIFICAR EL VALOR DEL STOCK+++")
        modificar(productos)
    elif opcion == 'e':
        print("+++ELIMINAR PRODUCTO CON STOCK CERO+++")  
        eliminar(productos)  
    elif opcion == 'f':
        print("+++PROGRAMA FINALIZADO+++")
    else:
        print("   ERROR ")
    input (" Presione cualquier letra...")  